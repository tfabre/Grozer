from shutil import copyfile
import os

files = ["index.html", "bundle.js"]
srcdir = "C:/IT/Grozer/dist"
destdir = "C:/IT/apache-tomcat-8.5.24/webapps/geoserver/data/www/grozer"

if os.name == 'nt':
	if not os.path.exists(destdir):
		os.mkdir(destdir)
	for i in range(0, len(files)):
		copyfile(srcdir+"/"+files[i], destdir+"/"+files[i])
