import GeoJSON from "ol/format/geojson";
import LayerVector from "ol/layer/vector";
import SourceVector from "ol/source/vector";
import Style from "ol/style/style";
import Stroke from "ol/style/stroke";
import Fill from "ol/style/fill";
import Circle from "ol/style/circle";

let geoJsonFormat = new GeoJSON();

function getRandomFrom0To(num)
{
    return Math.floor(Math.random() * num);
}

function getStyle(color)
{
    return feature =>
    {
        const type = feature.getGeometry().getType();
        if (type === "Point" || type === "MultiPoint")
        {
            return new Style({
                image: new Circle({
                    radius: 5,
                    fill: new Fill({
                        color: `rgba(${color}, 0.5)`
                    }),
                    stroke: new Stroke({color: color, width: 1})
                })
            });
        }
        else
        {
            return new Style({
                stroke: new Stroke({
                    color: `rgb(${color})`,
                    width: 1
                }),
                fill: new Fill({
                    color: `rgba(${color}, 0.5)`
                })
            });
        }
    };
}

export function createLayerWithRandomColor(layer)
{
    const color = `${getRandomFrom0To(256)}, ${getRandomFrom0To(256)}, ${getRandomFrom0To(256)}`;
    return new LayerVector({
        source: new SourceVector({
            features: geoJsonFormat.readFeatures(layer, { featureProjection : 'EPSG:3857' })
        }),
        style: getStyle(color)
    });
}
