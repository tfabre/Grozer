import $ from "jquery";
import Sortable from 'sortablejs/Sortable.min';

import { createLayerWithRandomColor } from "./LayerBuilder";

export class SortableLayer
{
    constructor(/** ol.map.Map */ map, /** LayerMap */ layersMap, /** ThemeFetcher */ layerFetcher)
    {
        // Dependencies:
        this.map = map;
        this.layersMap = layersMap;
        this.layerFetcher = layerFetcher;

        // The list of layers in the theme
        this.originalLayerList = null;
        // The list of layers which is displayed
        this.displayedLayerList = null;
        this.initSortableLayers();
    }

    initSortableLayers()
    {
        this.originalLayerList = Sortable.create(document.getElementById("layer-list"),
        {
            group: "couches",
            chosenClass: "active",
            ghostClass: "dropzoned"
        });

        this.displayedLayerList = Sortable.create(document.getElementById("selected-layer-list"),
        {
            group: "couches",
            chosenClass: "active",
            ghostClass: "dropzoned",
            onAdd: event => this.addLayerToView(event),
            onUpdate: event =>
            {
                const id = event.item.id;
                if (this.layersMap.containsKey(id))
                {
                    this.layersMap.move(event.oldIndex, event.newIndex);
                }
            },
            onRemove: event =>
            {
                const id = event.item.id;
                if (this.layersMap.containsKey(id))
                {
                    this.map.getLayers().remove(this.layersMap.get(id));
                    this.layersMap.delete(id);
                }
            }
        });
    }

    /**
     * Add a layer to the map view.
     * Check if layer is loaded, and delegate work to handleShow for display
     */
    addLayerToView(event)
    {
        const id =  event.item.id;
        const [wrkspace, theme, layerId] = id.split('-');
        const layer = this.layerFetcher.layerHierarchy[wrkspace][theme][layerId];

        if ($.isEmptyObject(layer))
        {
            this.layerFetcher.getLayer(wrkspace, theme, layerId).then(fetcher =>
            {
                this.handleShow(id,
                        fetcher.layerHierarchy[wrkspace][theme][layerId],
                        event.newIndex);
            });
        }
        else
        {
            this.handleShow(id, layer, event.newIndex);
        }

    }

    /**
     * Show effectively layer to the map from the layer data
     */
    handleShow(/** String */ id, /** GeoJSON */ layer, /** Int */ newIndex)
    {
        const vectorLayer = createLayerWithRandomColor(layer);

        this.layersMap.putAtIndex(id, vectorLayer, newIndex);
        this.map.getLayers().push(vectorLayer);

        // Center map on layer bounds
        this.map.getView().fit(vectorLayer.getSource().getExtent());
    }
}
