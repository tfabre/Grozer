export function hexToRGB_HSL(hex)
{
    let ret = {rgb:{}, hsl:{}};

    let hexmatch = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    let r = parseInt(hexmatch[1], 16);
    let g = parseInt(hexmatch[2], 16);
    let b = parseInt(hexmatch[3], 16);

    ret.rgb.r = r;
    ret.rgb.g = g;
    ret.rgb.b = b;

    //hsl
    let rgba = {};
    rgba.r = r / 255;
    rgba.g = g / 255;
    rgba.b = b / 255;
    let max = Math.max(rgba.r, rgba.g, rgba.b), min = Math.min(rgba.r, rgba.g, rgba.b), hsl = [], d;
    hsl.a = rgba.a;
    hsl.l = ( max + min ) / 2;
    if (max === min) {
        hsl.h = 0;
        hsl.s = 0;
    } else {
        d = max - min;
        hsl.s = hsl.l >= 0.5 ? d / ( 2 - max - min ) : d / ( max + min );
        switch (max) {
            case rgba.r:
                hsl.h = ( rgba.g - rgba.b ) / d + ( rgba.g < rgba.b ? 6 : 0 );
                break;
            case rgba.g:
                hsl.h = ( rgba.b - rgba.r ) / d + 2;
                break;
            case rgba.b:
                hsl.h = ( rgba.r - rgba.g ) / d + 4;
                break;
        }
        hsl.h /= 6;
    }
    hsl.h = parseInt(( hsl.h * 360 ).toFixed(0), 10);
    hsl.s = parseInt(( hsl.s * 100 ).toFixed(0), 10);
    hsl.l = parseInt(( hsl.l * 100 ).toFixed(0), 10);

    ret.hsl = {h:hsl.h, s:hsl.s, l:hsl.l};

    return ret;
}
