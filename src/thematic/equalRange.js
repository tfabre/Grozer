import Style from "ol/style/style";
import Stroke from "ol/style/stroke";
import Fill from "ol/style/fill";

import { colorFeaturesMonochromatic } from "./featureUtils";
import { colorFeaturesPolychromatic } from "./featureUtils";
import { colorFeaturesManual } from "./featureUtils";
import { quickSortFeaturesByValue } from "./featureUtils";

export function equalRange(classesNumber, features, sortValue, coloringType)
{
	let sortedFeatures = quickSortFeaturesByValue(features, sortValue);

	let minSortValue = sortedFeatures[0].values_[sortValue];
	let maxSortValue = sortedFeatures[sortedFeatures.length-1].values_[sortValue];
	let rangeValue = Math.ceil((maxSortValue - minSortValue) / classesNumber);

	//Coloring type: Monochromatic, Polychromatic, Manual
	if (coloringType === "mono")
	{
		if (minSortValue > 0)
		{
			var featuresColors = colorFeaturesMonochromatic(classesNumber, false);
		}
		else
		{
			var featuresColors = colorFeaturesMonochromatic(classesNumber, true);
		}
	}
	else if (coloringType === "poly")
	{
		if (minSortValue > 0)
		{
			var featuresColors = colorFeaturesPolychromatic(classesNumber, false);
		}
		else
		{
			var featuresColors = colorFeaturesPolychromatic(classesNumber, true);
		}
	}
	else
	{
		var featuresColors = colorFeaturesManual(coloringType);
	}

	let currentClassValue = minSortValue + rangeValue;
	let currentClassFeatures = [];
	let sortedClasses = [];

	for (let i = 0; i < sortedFeatures.length; i++)
	{
		if (sortedFeatures[i].values_[sortValue] <= currentClassValue)
		{
			currentClassFeatures.push(sortedFeatures[i]);
		}
		else
		{
			while (sortedFeatures[i].values_[sortValue] > currentClassValue)
			{
				currentClassValue = currentClassValue + rangeValue;
				sortedClasses.push(currentClassFeatures);
				currentClassFeatures = [];
			}
			currentClassFeatures.push(sortedFeatures[i]);
		}
		if (i === sortedFeatures.length-1)
		{
			sortedClasses.push(currentClassFeatures);
		}
	}

	let k = 0;
	for (let i = 0; i < sortedClasses.length; i++)
	{
		let strokeColor = featuresColors[k];
		let fillColor = featuresColors[k+1];
		k = k + 2;

		for (let j = 0; j < sortedClasses[i].length; j++)
		{
			sortedClasses[i][j].setStyle(new Style({
	            stroke: new Stroke({
	                color: strokeColor,
	                width: 1
	            }),
	            fill: new Fill({
	                color: fillColor
	            })
	        }));
		}
	}
}
