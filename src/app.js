import $ from "jquery";
import Map from "ol/map";
import Tile from "ol/layer/tile";
import OSM from "ol/source/osm";
import View from "ol/view";
import LayerVector from "ol/layer/vector";
import SourceVector from "ol/source/vector";
import Style from "ol/style/style";
import Stroke from "ol/style/stroke";
import Fill from "ol/style/fill";
import Coordinate from "ol/coordinate";
import Proj from "ol/proj";
import Control from "ol/control";
import Scaleline from "ol/control/scaleline";
import Overlay from "ol/overlay";
import OverviewMap from "ol/control/overviewmap";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import 'ol/ol.css';
import './css/style.css';

import { ThemeFetcher } from "./layers/ThemeFetcher";
import { LayersMap } from "./layers/LayersMap";
import { intersection } from "./spatial/intersection";
import { equalRange } from "./thematic/equalRange";
import { standardized } from "./thematic/standardized";

import { SortableLayer } from "./ui/SortableLayer";
import { ThemeSwitcher } from "./ui/ThemeSwitcher";

import { ExportLayer } from "./export/ExportLayer";
import { hexToRGB_HSL } from "./utils/Color";

$(() =>
{
    var scaleLineControl = new Scaleline;
    // Displayed map
    let map = new Map({
        controls: Control.defaults({
            attributionOptions: {
                collapsible: false
            }
        }).extend([
            scaleLineControl, new OverviewMap()
        ]),
        layers: [
            new Tile({
                source: new OSM()
            }),
        ],
        target: 'map',
        view: new View({
            center: [0, 0],
            zoom: 2
        })
    });

    // Contains information of displayed layers
    let layersMap = new LayersMap();

    // Fetch layers informations
    let fetcher = new ThemeFetcher();
    // Fetch workspaces and init the view with the TOPP workspace
    // and load the main (=> default) workspace of the connected user
    fetcher.update = () =>
        fetcher.getWrkspaces().then(fetcher =>
        {
            fetcher.getDefaultWrkspaceName().then(currentWorkspace =>
            {
                new ThemeSwitcher(map, layersMap, fetcher, currentWorkspace);
                new ExportLayer(fetcher, currentWorkspace);
            });
        });
    fetcher.update();

    $("#intersection").click(() =>
    {
        if (layersMap.containsKey('intersection'))
        {
            map.getLayers().remove(layersMap.intersection);
            layersMap.delete('intersection');
            // layersMap['intersection'] = undefined;
        }
        else
        {
            if (layersMap.layerStack.stack.length >= 2)
            {
                let intersectionLayer = new LayerVector({
                    source: new SourceVector(),
                    style: new Style({
                        stroke: new Stroke({
                            color: 'green',
                            width: 1
                        }),
                        fill: new Fill({
                            color: 'rgba(0, 255, 0, 0.75)'
                        })
                    })
                });

                intersection(layersMap.layerStack.stack[0].getSource().getFeatures(),
                             layersMap.layerStack.stack[1].getSource().getFeatures())
                    .forEach(inter => intersectionLayer.getSource().addFeature(inter));
                map.getLayers().push(intersectionLayer);
                layersMap.put('intersection', intersectionLayer);
            }
            else
            {
                alert("Vous demandez l'intersection alors que vous n'avez pas affiché au moins 2 couches.\nOpération refusée.");
            }
        }
    });

    $("#equal-range").click(() =>
    {
        if (layersMap.containsKey('equalRange'))
        {
            map.getLayers().remove(layersMap.equalRange);
            layersMap.delete('equalRange');
        }
        else
        {
            let layer = layersMap.layerStack.stack[0]; // layers.item(layersLength -1);
            let features = layer.getSource().getFeatures();

            let equalRangeLayer = new LayerVector({
                source: new SourceVector(),
                style: new Style({
                    stroke: new Stroke({
                        color: 'green',
                        width: 1
                    }),
                    fill: new Fill({
                        color: 'rgba(0, 255, 0, 0.1)'
                    })
                })
            });

            //Coloring type: Monochromatic, Polychromatic, Manual
            let colortype;
            if ($("input[name=colortype]:checked").val() === "manuel")
            {
                colortype = getSelectedColors();
            }
            else
            {
                colortype = $("input[name=colortype]:checked").val();
            }
            equalRange($("#input_nb_classes_color").val(), features, $("#featuresAttributes").val(), colortype)
                .forEach(ft => equalRangeLayer.getSource().addFeature(ft));
            map.getLayers().push(equalRangeLayer);
            layersMap.put('equalRange', equalRangeLayer);
        }
    });

    $("#standardized").click(() =>
    {
        if (layersMap.containsKey('standardized'))
        {
            map.getLayers().remove(layersMap.standardized);
            layersMap.drop('standardized');
        }
        else
        {
            let layer = layersMap.layerStack.stack[0]; // layers.item(layersLength -1);
            let features = layer.getSource().getFeatures();

            let standardizedLayer = new LayerVector({
                source: new SourceVector(),
                style: new Style({
                    stroke: new Stroke({
                        color: 'green',
                        width: 1
                    }),
                    fill: new Fill({
                        color: 'rgba(0, 255, 0, 0.1)'
                    })
                })
            });

            //Coloring type: "Monochromatic", "Polychromatic", or manual: pass array of colors instead
            let colortype;
            if ($("input[name=colortype]:checked").val() === "manuel")
            {
                colortype = getSelectedColors();
            }
            else
            {
                colortype = $("input[name=colortype]:checked").val();
            }
            standardized($("#input_nb_classes_color").val(), features, $("#featuresAttributes").val(), colortype)
                .forEach(ft => standardizedLayer.getSource().addFeature(ft));
            map.getLayers().push(standardizedLayer);
            layersMap.put('standardized', standardizedLayer);
        }
    });


    // Initialise layer list
    let sortableLayers = new SortableLayer(map, layersMap, fetcher);

    $("#button-export-current-layers").on("click", function()
    {
        $("#popup-export-list-couche-selected").empty();
        $("#list-couche-selected").clone().appendTo("#popup-export-list-couche-selected");
    });

    $("#input_nb_classes_color").on("change", function()
    {
        let val = parseInt(this.value);
        let old = parseInt(this.dataset.old);

        if(old == -1)
            for(let i = 1; i<= val; i++)
                $("<input>").attr({type:"color",id:`classe_color_${i}`, name:"manuel", style:"margin-right: 4px"}).appendTo("#list_color_class");

        else if(val > old)
        {
            for( let i = old+1; i <= val; i++)
            {
                let elem = document.getElementById("classe_color_"+i);
                if(elem == null)
                    $("<input>").attr({type:"color",id:`classe_color_${i}`, name:"manuel", style:"margin-right: 4px"}).appendTo("#list_color_class");
            }
        }
        else
        {
            for(let i = val+1; i <= old; i++)
            {
                let elem = document.getElementById("classe_color_"+i).remove();
            }
        }

        this.dataset.old = val;
    });

    $("input[name=colortype]", '#thematic-analyze-div').on("change", function()
    {
        if(this.value == "manuel")
            $("input[name=manuel]").prop("disabled", false);
        else
            $("input[name=manuel]").prop("disabled", true);

    });


    $("#button-spatial-request").on("click", function()
    {
        let attrs = getAttributesSelectedFeatures();

        $("#featuresAttributes").empty();


        attrs.forEach(function(a)
        {
            a != "geometry" && $("<option>", {value: a, text: a}).appendTo("#featuresAttributes");
        });

        let s = $("#featuresAttributes option").get(0).value;

        $("#featuresAttributes option[value="+s+"]").attr("selected",  true);
    });


    function getAttributesSelectedFeatures()
    {
        let featuresKey = new Set();
        const layers = map.getLayers();

        let layer = layersMap.layerStack.stack[0]; // layers.item(layers.getLength()-1);

        let features = layer.getSource().getFeatures();
        features.forEach(function(f)
        {
            for (let objectKey in f.values_)
                featuresKey.add(objectKey);
        });

        return featuresKey;
    }

    //return and array of selected colors [{rgb:{r,g,b}, hsl:{h,s,l}}]
    function getSelectedColors()
    {
        let color_selectors = $(`input[name=manuel]`); // get all inputs

        let hexs = [];
        let rgb_hsl = [];

        color_selectors.each(function(i, elem)
        {
            let hex = elem.value;
            hexs.push(hex);
            rgb_hsl.push(hexToRGB_HSL(hex));
        });

        return rgb_hsl;
    }

    var container = document.getElementById('popup');

    var popup = new Overlay({
        element: container
    });

    map.addOverlay(popup);

    map.on('click', function(evt) {

        var coordinate = evt.coordinate;
        var hdms = Coordinate.toStringHDMS(Proj.transform(
            coordinate, 'EPSG:3857', 'EPSG:4326'));

        $(container).popover('dispose');
        popup.setPosition(coordinate);
        // the keys are quoted to prevent renaming in ADVANCED mode.
        displayFeatureInfo(evt.pixel);
    });

    var displayFeatureInfo = function(pixel)
    {
        var features = [];
        var contenu = '';
        let compteur = 0;

        map.forEachFeatureAtPixel(pixel, function(feature, layer)
        {
            features.push(feature);
        });
        if (features.length > 0)
        {

            var info = [];
            for (var i = 0, ii = features.length; i < ii; ++i)
            {
                info.push(features[i].values_);

                console.log(info);
                console.log(features[i]);
            }

            info.forEach(function(element)
            {
                console.log(element);
                contenu+='<div class=<p> ID: ' + features[compteur].id_ + "</p>";
                contenu+= "<hr>";
                for(var key in element)
                {
                    if(key!='geometry')
                        contenu += "<p>" + key + ": "+ element[key] + "</p>";
                }
                contenu+= "<hr>";

                compteur++;
            });

            $(container).popover
            ({
                'placement': 'top',
                'animation': false,
                'html': true,
                'content': contenu
            });
            $(container).popover('show');
        }
    };
});
