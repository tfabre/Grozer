import LoadingImg from "../img/loader.gif";

export class LoadingAnimation
{
    constructor()
    {
        this.loadingImg = new Image();
        this.loadingImg.src = LoadingImg;
    }

    appendToElementWithId(parentId)
    {
        document.getElementById(parentId).appendChild(this.loadingImg);
    }

    remove()
    {
        this.loadingImg.parentElement.removeChild(this.loadingImg);
    }
}
