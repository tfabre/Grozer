/**
 * Layer stack to keep index and Z-index of displayed layers
 */
export class LayerStack
{
    constructor()
    {
        this.stack = [/** {ol.layer.Layer} */];
    }

    insertAtIndex(/** {Int} */ index, /** {ol.layer.Layer} */ layer)
    {
        this.stack.splice(index, 0, layer);
        this.reorganizeLayers();
    }

    dropFromIndex(/** {Int} */ index)
    {
        this.stack.splice(index, 1);
        this.reorganizeLayers();
    }

    move(/** {Int} */ oldIndex, /** {Int} */ newIndex)
    {
        const [layer] = this.stack.splice(oldIndex, 1);
        this.stack.splice(newIndex, 0, layer);
        this.reorganizeLayers();
    }

    reorganizeLayers()
    {
        const layerStackSize = this.stack.length;
        this.stack.forEach((elem, index) =>
        {
            elem.setZIndex(((index +1) * (layerStackSize -1)) % layerStackSize);
        });
    }

    clear()
    {
        this.stack = [];
    }
}
